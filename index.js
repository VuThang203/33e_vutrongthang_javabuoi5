// Cau 1:
const khuA = "khuA";
const khuB = "khuB";
const khuC = "khuC";
const doiTuong1 = "doiTuong1";
const doiTuong2 = "doiTuong2";
const doiTuong3 = "doiTuong3";
function diemUuTienKhuVuc(d){
    if(d == khuA){
        return 2;
    }
    if(d== khuB){
        return 1;
    }
    if(d== khuC){
        return 1.5;
    }
}
function diemUuTienDoiTuong(e){
    if(e == doiTuong1){
        return 2.5;
    }
    if(e == doiTuong2){
        return 1.5;
    }
    if(e == doiTuong3){
        return 1;
    }
}
function ketQuaTuyenSinh(){
    event.preventDefault();
    var c = document.getElementById("diemchuan").value*1;
    var d = document.getElementById("khuvuc").value;
    var e = document.getElementById("doituong").value;
    var f = document.getElementById("diem-thu-nhat").value *1;
    var g = document.getElementById("diem-thu-hai").value *1;
    var h = document.getElementById("diem-thu-ba").value *1;
    var diemKhuVuc = diemUuTienKhuVuc(d);
    var diemDoiTuong = diemUuTienDoiTuong(e);
    var t = "";
    var k = (f+g+h+diemKhuVuc+diemDoiTuong); 
    if(k>=c && ((f,g,h)!==0)){
        t = `Bạn đã đậu. Tổng điểm là: ` + k;
    }else{
        t = `Bạn đã rớt. Tổng điểm là: ` +k;
    } 
    if(f==0||g==0||h==0){
        t = `Bạn đã rớt do có điểm bằng 0`;
    }
    document.getElementById("result-1").innerHTML = t;
}
// Cau 2:

function tinhTienDien(){
    event.preventDefault();
    var hoTen = document.getElementById("txt-ho-ten").value;
    var soKw =  document.getElementById("txt-so-kw").value*1;
    var t = "";
    if(1<=soKw && soKw <=50){
        t = `Họ tên: ` + hoTen + `. Số tiền cần phải trả là: ` + (new Intl.NumberFormat('vi-VN').format(500 *soKw))+ `VND`;
    }else if(50<soKw && soKw<=100){
        t = `Họ tên: ` + hoTen + `. Số tiền cần phải trả là: ` + (new Intl.NumberFormat('vi-VN').format(650*soKw)) + `VND`;
    }else if(100<soKw && soKw<=200){
        t = `Họ tên: ` + hoTen + `. Số tiền cần phải trả là: ` + (new Intl.NumberFormat('vi-VN').format(850*soKw)) + `VND`;
    }else if(200<soKw && soKw<=350){
        t = `Họ tên: ` + hoTen + `. Số tiền cần phải trả là: ` + (new Intl.NumberFormat('vi-VN').format(1100*soKw)) + `VND`;
    }else if(soKw>350){
        t = `Họ tên: ` + hoTen + `. Số tiền cần phải trả là: ` + (new Intl.NumberFormat('vi-VN').format(1300*soKw)) + `VND`;
    }else{
        t = `Số Kw không hợp lệ`;
    }
    document.getElementById("result-2").innerHTML = t;
}